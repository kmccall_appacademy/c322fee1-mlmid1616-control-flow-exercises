# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  only_uppercase = ""
  str.chars.each do |letter|
    if letter.downcase != letter
    only_uppercase << letter
    end
  end
  only_uppercase
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  half_length = str.length / 2
  if str.length.even?
    return str[half_length - 1 ] + str[half_length]
  else
    return str[str.length / 2 ]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count {|letter| VOWELS.include?(letter)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_string = ""
  (0..arr.length-1).each do |idx|
  if idx < arr.length - 1
    new_string << arr[idx] + separator
  else
    new_string << arr[idx]
  end
end
  new_string
end


# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ""
  str.chars.each_with_index do |letter,idx|
    if idx.even?
      new_string << letter.downcase
    else
      new_string << letter.upcase
    end
  end
  new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_string = ""
  word_array = str.split
  word_array.each_with_index do |word,idx|
    if word.length > 4
      idx == word_array.length - 1 ? new_string << word.reverse : new_string << word.reverse + " "
    elsif idx < word_array.length - 1
      new_string << word + " "
    else
      new_string << word
    end
  end
  new_string
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  new_array = (1..n).to_a
  (1..n).to_a.each_with_index do |num,idx|
    if num % 3 == 0 && num % 5 == 0
      new_array[idx] = "fizzbuzz"
    elsif num % 3 == 0
      new_array[idx] = "fizz"
    elsif num % 5 == 0
      new_array[idx] = "buzz"
    else
      new_array[idx] = num
    end
  end
  new_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_array = []
  arr.each {|num| new_array << num.to_s.reverse.to_i}
  new_array.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  possible_factors = (2...num)
  how_many_factors = possible_factors.count {|divisor| num % divisor == 0}
  how_many_factors >= 1 ? false : true
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  possible_factors = (1..num)
  possible_factors.each do |possible_factor|
    factors << possible_factor if num % possible_factor == 0
  end
  factors.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors(num).each do |possible_prime|
    prime_factors << possible_prime if prime?(possible_prime)
  end
  prime_factors.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr[-1].odd? && arr[-2].odd?
    arr.each {|num| return num if num.even?}
  elsif arr[-1].even? && arr[-2].even?
    arr.each {|num| return num if num.odd?}
  elsif arr[0].odd?
    arr.each {|num| return num if num.even?}
  else
    arr.each {|num| return num if num.odd?}
end

end
